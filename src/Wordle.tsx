import { useState, useEffect } from 'react'
import { useShallow } from 'zustand/react/shallow'
import { Alert } from './components/alerts/Alert'
import { OverflowGrid } from './components/grid/OverflowGrid'
import { Keyboard } from './components/keyboard/Keyboard'
import {
  NOT_ENOUGH_LETTERS_MESSAGE,
  WORD_NOT_FOUND_MESSAGE
} from './constants/strings'
import { MAX_WORD_LENGTH } from './constants/settings'
import { getGuessValue } from './lib/statuses'
import { isWordInWordList, isWinningWordFromSeed } from './lib/words'

import './App.css'
import { useGameStore } from './game_state'

const ALERT_TIME_MS = 2000

type Props = {
  animating: boolean,
  chooseAction: () => void,
  processGuess: (guess: string, guessValue: number) => void
}

export const Wordle = ({ animating, chooseAction, processGuess }: Props) => {
  const { seed, guesses, guessOrder } = useGameStore(
    useShallow((state) => ({ seed: state.seed, guesses: state.guesses, guessOrder: state.guessOrder }))
  )

  const [currentGuess, setCurrentGuess] = useState('')

  const [isGameWon, setIsGameWon] = useState(false)
  const [isGameLost, setIsGameLost] = useState(false)

  const [isNotEnoughLetters, setIsNotEnoughLetters] = useState(false)
  const [isWordNotFoundAlertOpen, setIsWordNotFoundAlertOpen] = useState(false)

  const [enterPressed, setEnterPressed] = useState(false)

  useEffect(() => {
    if (isGameWon) {
      setTimeout(() => {
        setIsGameWon(false)
      }, ALERT_TIME_MS)
    }
    if (isGameLost) {
      setTimeout(() => {
        setIsGameLost(false)
      }, ALERT_TIME_MS)
    }
  }, [isGameWon, isGameLost])

  const onChar = (value: string) => {
    if (animating) return

    if (
      currentGuess.length < MAX_WORD_LENGTH &&
      !isGameWon
    ) {
      setCurrentGuess(`${currentGuess}${value}`)
    }
  }

  const onDelete = () => {
    setCurrentGuess(currentGuess.slice(0, -1))
  }

  const onEnter = () => {
    if (animating || enterPressed) return
    setEnterPressed(true)

    setTimeout(() => {
      setEnterPressed(false)
    }, 3000)

    if (isGameWon || isGameLost) {
      processGuess(currentGuess, getGuessValue(currentGuess, guesses))
      return
    }
    if (!(currentGuess.length === MAX_WORD_LENGTH)) {
      setIsNotEnoughLetters(true)
      return setTimeout(() => {
        setIsNotEnoughLetters(false)
      }, ALERT_TIME_MS)
    }

    if (!isWordInWordList(currentGuess)) {
      setIsWordNotFoundAlertOpen(true)
      return setTimeout(() => {
        setIsWordNotFoundAlertOpen(false)
      }, ALERT_TIME_MS)
    }

    const winningWord = isWinningWordFromSeed(currentGuess, seed)

    if (
      currentGuess.length === MAX_WORD_LENGTH &&
      !isGameWon
    ) {
      processGuess(currentGuess, getGuessValue(currentGuess, guesses))
      setCurrentGuess('')

      if (winningWord) {
        return setIsGameWon(true)
      }
    }
  }

  return (
    <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <OverflowGrid guesses={guesses} guessOrder={guessOrder} currentGuess={currentGuess} chooseAction={chooseAction} />
      <Keyboard
        onChar={onChar}
        onDelete={onDelete}
        onEnter={onEnter}
        guesses={guesses}
      />
      <Alert message={NOT_ENOUGH_LETTERS_MESSAGE} isOpen={isNotEnoughLetters} />
      <Alert
        message={WORD_NOT_FOUND_MESSAGE}
        isOpen={isWordNotFoundAlertOpen}
      />
    </div>
  )
}
