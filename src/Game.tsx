import { useState, useEffect } from 'react'
import { Alert } from './components/alerts/Alert'
import { BattleRow } from './components/battle/BattleRow'
import { Menu } from './Menu'
import { Store } from './components/store/Store'
import { Wordle } from './Wordle'
import { ActionModal } from './components/modals/ActionModal'
import { NextRoundModal } from './components/modals/NextRoundModal'
import { CharacterProfileModal } from './components/modals/CharacterProfileModal'
import { GameOverModal } from './components/modals/GameOverModal'
import { getWordFromSeed } from './lib/words'
import {
  loadBattleStateFromLocalStorage,
  loadBestStreakFromLocalStorage,
  saveStreak,
  unit
} from './lib/localStorage'
import { BASE_PLAYER } from './constants/units'
import sword from './assets/dagger.png'
import poison_sword from './assets/store/PoisonDagger.png'

import './App.css'
import { useGameStore, useBattleStore } from './game_state'
import { processEnemyGuess, processGuess } from './lib/game_logic'

type Props = {
    soundEffects: boolean
}

export const Game = ({ soundEffects = false }: Props) => {
  const [bestStreak, setBestStreak] = useState<number>(loadBestStreakFromLocalStorage())
  const {seed, guesses, addGuess, newWord } = useGameStore(
    (state) => ({ 
      seed: state.seed, guesses: state.guesses, 
      setSeed: state.setSeed, addGuess: state.addGuess, newWord: state.newWord })
  )

  const { 
    playerTurn, round, money, player, enemy, 
    newGame, nextRound, setPlayerTurn, setPlayer, setEnemy, setMoney } = useBattleStore(
    (state) => ({
      playerTurn: state.playerTurn, round: state.round, money: state.money, player: state.player, enemy: state.enemy,
      newGame: state.newGame, nextRound: state.nextRound, 
      setPlayerTurn: state.setPlayerTurn, setPlayer: state.setPlayer, setEnemy: state.setEnemy, setMoney: state.setMoney
    })
  )

  const [inBattle, setInBattle] = useState(false)
  const [inStore, setInStore] = useState(false)

  const [animating, setAnimating] = useState(false)
  const [animation, setAnimation] = useState({})
  const [roundStart, setRoundStart] = useState(false)

  const [avatar, setAvatar] = useState<string>(() => {
    const loaded = loadBattleStateFromLocalStorage()

    return loaded ? loaded.player.avatar : BASE_PLAYER.avatar
  })
  const [isActionModalOpen, setIsActionModalOpen ] = useState(false)
  const [isNextRoundModalOpen, setIsNextRoundModalOpen] = useState(false)
  const [isCharacterPofileOpen, setIsCharacterProfileOpen] = useState(false)
  const [isGameOverModalOpen, setIsGameOverModalOpen] = useState(false)

  const [selectedCharacter, setSelectedCharacter] = useState<unit>(BASE_PLAYER)

  const [missedWord, setMissedWord] = useState("")
  const [isMissedWordOpen, setIsMissedWordOpen] = useState(false)
  const [moneyEarned, setMoneyEarned] = useState("")
  const [isMoneyEarnedOpen, setIsMoneyEarnedOpen] = useState(false)

  const [playerAttackSound] = useState(new Audio('attack1.wav'))
  const [enemyAttackSound] = useState(new Audio('attack2.wav'))
  const [winSound] = useState(new Audio('win.wav'))

  const executeEnemyAI = async () => {
    await handleEnemyGuess()
  }

  useEffect(() => {
    checkBattleConditions()
    saveStreak(bestStreak)

    if (!playerTurn && !isNextRoundModalOpen && 
      !guesses.includes(getWordFromSeed(seed).solution) && !animating && 
      player.current > 0 && enemy.current > 0) {
      setTimeout(() => {
        executeEnemyAI()
      }, 2000)
    }
  }, [guesses, animating])

  const startGame = () => {
    setInBattle(true)
    newGame(avatar)
    newWord()

  }

  const continueGame = () => {
    setInBattle(true)
  }

  const chooseAction = () => {
    setIsActionModalOpen(true)
  }

  const handleActionSelection = (value: string) => {
    setPlayer({
      ...player,
      action: value
    })
    setIsActionModalOpen(false)
  }

  const handleGuess = async (guess: string, guessValue: number) => {
    if (!playerTurn || animating || isNextRoundModalOpen || inStore || isCharacterPofileOpen) return

    let summary = await processGuess({
        action: player.action,
        guess: guess,
        guessValue: guessValue,
        guesses: guesses,
        solution: getWordFromSeed(seed).solution,
        player: player,
        enemy: enemy
    })

    setPlayer(summary.player)
    setEnemy(summary.enemy)
    setAnimation({ player: summary.player_animation, enemy: summary.enemy_animation})

    if (summary.player_animation.length > 0 || summary.enemy_animation.length > 0) {
        setAnimating(true)
    }

    if (roundStart && playerTurn && player.equipped.includes('SpeedyBoots')) {
      setRoundStart(false)
    } else {
      setPlayerTurn(!playerTurn)
    }
    addGuess(summary.guess, "player")

    if (soundEffects) {
      if (playerTurn) {
        playerAttackSound.play() 
      } else {
        enemyAttackSound.play()
      }
    }

    if (summary.guess.toUpperCase() === getWordFromSeed(seed).solution) {
      setTimeout(() => {
        handleRoundFinish(summary.guess.toUpperCase() === getWordFromSeed(seed).solution)
      }, 1000)
    } else {
      setTimeout(() => {
        checkBattleConditions()
      }, 500)
    }
  }

  const handleEnemyGuess = async () => {
    if (animating || isNextRoundModalOpen || inStore || isCharacterPofileOpen) return

    // let aiAction = chooseAiAction(enemy, guesses)
    // setEnemy({
    //   ...enemy,
    //   action: aiAction
    // })

    // let aiGuess = chooseWord(guesses)

    let summary = await processEnemyGuess({
        action: player.action,
        guess: "",
        guessValue: 0,
        guesses: guesses,
        solution: getWordFromSeed(seed).solution,
        player: player,
        enemy: enemy
    })

    setPlayer(summary.player)
    setEnemy(summary.enemy)
    setAnimation({ player: summary.player_animation, enemy: summary.enemy_animation})

    if (summary.player_animation.length > 0 || summary.enemy_animation.length > 0) {
        setAnimating(true)
    }

    setPlayerTurn(true)
    addGuess(summary.guess.toUpperCase(), "enemy")

    if (soundEffects) {
        if (playerTurn) {
          playerAttackSound.play() 
        } else {
          enemyAttackSound.play()
        }
      }

    if (summary.guess.toUpperCase() === getWordFromSeed(seed).solution) {
        setTimeout(() => {
            handleRoundFinish(summary.guess.toUpperCase() === getWordFromSeed(seed).solution)
        }, 1000)
    } else {
        checkBattleConditions()
    }
  }

  const handleRoundFinish = (solved: boolean) => {
    if (!solved) {
      setMissedWord("The word was " + getWordFromSeed(seed).solution)
      setIsMissedWordOpen(true)
      setTimeout(() => {
        setIsMissedWordOpen(false)
      }, 2000)
    } else if (playerTurn) {
      let multiplier = player.equipped.includes('GoldPot') ? 2 : 1
      let earnings = (7 - guesses.length) * 10
      setMoney(money + (earnings * multiplier))
      setMoneyEarned("You Earned $" + (earnings * multiplier))
      setIsMoneyEarnedOpen(true)
      setTimeout(() => {
        setIsMoneyEarnedOpen(false)
      }, 2000)
    }

    setTimeout(() => {
      newWord()

      checkBattleConditions()
    }, 1500)
  }

  const checkBattleConditions = () => {
    if (player.current <= 0) {
      setTimeout(() => {
        setInBattle(false)
      }, 1500)
      if (round > bestStreak) {
        saveStreak(round)
        setBestStreak(round)
      }
      setIsGameOverModalOpen(true)
      setPlayer({
        ...BASE_PLAYER,
        current: 0,
      })
      return
    }

    if (enemy.current <= 0) {
      soundEffects && winSound.play()
      let multiplier = player.equipped.includes('GoldPot') ? 2 : 1
      setPlayerTurn(true)
      setMoney(money + (10 * multiplier))
      setMoneyEarned("You Earned $" + (10 * multiplier))
      setIsMoneyEarnedOpen(true)
      setTimeout(() => {
        setIsMoneyEarnedOpen(false)
      }, 2000)
      setIsNextRoundModalOpen(true)

      nextRound()
      setRoundStart(true)
      if (!isMissedWordOpen) {
        setMissedWord("The word was " + getWordFromSeed(seed).solution)
        setIsMissedWordOpen(true)
        setTimeout(() => {
          newWord()
          setIsMissedWordOpen(false)
        }, 2000)
      }
    }
  }

  const startNextRound = () => {
    newWord()
    setIsNextRoundModalOpen(false)
  }

  const updateAvatar = (value: string) => {
    setAvatar(value)
  }

  const stopAnimation = () => {
    setAnimation({})
    setAnimating(false)
  }

  const visitStore = () => {
    if (player.equipped.includes('VampireCloak')) {
      setPlayer({
        ...player,
        current: player.current - 2 > 1 ? player.current - 2 : 1,
      })
    }

    setInStore(true)
    setIsNextRoundModalOpen(false)
    // setSeed(Math.floor(Math.random() * 1000000))
    newWord()
  }

  const leaveStore = () => {
    setInStore(false)
    setIsNextRoundModalOpen(true)
  }

  const viewCharacter = (unit: unit) => {
    setSelectedCharacter(unit)
    setIsCharacterProfileOpen(true)
  }

  const handleEquip = (id: string) => {
    setPlayer({
        ...player,
        equipped: [...player.equipped, id],
        action: player.action === sword && id === "PoisonDagger" ? poison_sword : player.action
    })
  }

  const handleUnEquip = (id: string) => {
    setPlayer({
        ...player,
        equipped: player.equipped.filter((item) => item !== id),
        action: player.action === poison_sword && id === "PoisonDagger" ? sword : player.action
    })
  }

  return (
    <div className="py-4 max-w-7xl mx-auto sm:px-6 lg:px-8">
      {inBattle ? 
        (!inStore ?
          <>
            <BattleRow
              animation={animation}
              stopAnimation={stopAnimation}
              characterClick={viewCharacter}
            />
            <Wordle 
              animating={animating || !playerTurn}
              chooseAction={chooseAction} 
              processGuess={handleGuess}
            />
          </> :
          <>
            <Store
              leave={leaveStore} 
            />
          </>) :
        <Menu
          play={startGame} 
          continueGame={continueGame}
          updateAvatar={updateAvatar}/>
      }
      <ActionModal
        isOpen={isActionModalOpen}
        handleClose={() => setIsActionModalOpen(false)}
        selectAction={handleActionSelection}
      />
      <CharacterProfileModal 
        unit={selectedCharacter}
        isOpen={isCharacterPofileOpen}
        handleClose={() => setIsCharacterProfileOpen(false)}
        equip={handleEquip}
        unequip={handleUnEquip}
      />
      <NextRoundModal 
        round={round}
        visitStore={visitStore}
        nextRound={startNextRound}
        isOpen={isNextRoundModalOpen}
        handleClose={startNextRound}
      />
      <GameOverModal 
        round={round} 
        bestStreak={bestStreak} 
        isOpen={isGameOverModalOpen}
        handleClose={() => setIsGameOverModalOpen(false)}
      />
      <Alert
          message={missedWord}
          isOpen={isMissedWordOpen}
      />
      <Alert
          variant='success'
          message={moneyEarned}
          isOpen={isMoneyEarnedOpen}
      />
    </div>
  )
}

export default Game
