import { unit } from './localStorage'
import sword from '../assets/dagger.png'

const calculateVampireSiphon = (unit: unit, guessValue: number): number => {
    if (guessValue === 0) return 0

    return Math.min(2 * unit.level, guessValue)
}

const hasPoisonDagger = (items: string[]): boolean => {
    return items.includes('PoisonDagger') || items.includes('PoisonDagger2')
  }

const calculatePoisonDamage = (unit: unit): number => {
    return unit.equipped.includes('PoisonDagger2') ? 2 : 1
}

export const processAttack = (attacker: unit, attackee: unit, guessValue: number) => {
    let updates = []
    let scaledGuessValue = guessValue - (attackee.afflictions.includes('reflect') ? guessValue : 0)

    updates.push({ 
        update: 'shields', 
        type: attackee.name, 
        value: Math.max(0, attackee.shields - scaledGuessValue)})

    let remainingAttack = Math.max(0, scaledGuessValue - attackee.shields)
    let attackeeCurrent = Math.max(0, attackee.current - remainingAttack)

    updates.push({ 
        update: 'current', 
        type: attackee.name, 
        value: attackeeCurrent})

    let hadReflect = attackee.afflictions.includes('reflect')
    let attackeeAfflictions = attackee.afflictions.filter((affliction) => affliction !== 'reflect')

    updates.push({
        update: 'afflictions',
        type: attackee.name,
        value: hasPoisonDagger(attacker.equipped) && !attackeeAfflictions.includes('poison') ? 
            [...attackeeAfflictions, 'poison'] : attackeeAfflictions
    })

    let attackerCurrent = attacker.current

    if (attacker.equipped.includes('VampireCloak')) {
        attackerCurrent = Math.min(attacker.total, attacker.current + calculateVampireSiphon(attacker, remainingAttack))
    }

    if (attacker.afflictions.includes('poison') && guessValue > 0) {
        attackerCurrent = attackerCurrent - calculatePoisonDamage(attackee)
    }

    if (hadReflect) {
        attackerCurrent = Math.max(0, attackerCurrent - (guessValue * 2))
    }

    updates.push({ 
        update: 'current', 
        type: attacker.name, 
        value: attackerCurrent
    })
    updates.push({
        update: 'shields',
        type: attacker.name,
        value: attacker.shields
    })
    updates.push({
        update: 'afflictions',
        type: attacker.name,
        value: attacker.afflictions
    })

    return updates
}

export const processShields = (unit: unit, guessValue: number) => {
    return [
        { update: 'shields', type: unit.name, value: unit.shields + guessValue }
    ]
}

export const processHeal = (unit: unit, guessValue: number) => {
    let bandageCount = Math.max(0, unit.bandages - 1)
    return [
        { update: 'current', type: unit.name, value: Math.min(unit.total, unit.current + 3 + guessValue) },
        { update: 'bandages', type: unit.name, value: bandageCount }, 
        { update: 'action', type: unit.name, value: bandageCount === 0 ? sword : unit.action }
    ]
}

export const processJester = (attacker: unit, attackee: unit, guessValue: number) => {
    let updates = []
    
    let attackerCurrent = Math.max(0, attacker.current - guessValue)

    if (attacker.equipped.includes('VampireCloak')) {
        attackerCurrent = Math.min(attacker.total, attacker.current + calculateVampireSiphon(attacker, guessValue))
    }

    if (attacker.afflictions.includes('poison') && guessValue > 0) {
        attackerCurrent = attackerCurrent - calculatePoisonDamage(attackee)
    }

    let attackeeHadReflect = attackee.afflictions.includes('reflect')
    let attackeeAfflictions = attackee.afflictions
    if (attackeeHadReflect) {
        attackerCurrent = Math.max(0, attackerCurrent - (guessValue * 4))
        attackeeAfflictions = attackeeAfflictions.filter((affliction) => affliction !== 'reflect')
    }

    updates.push({ 
        update: 'shields', 
        type: attacker.name, 
        value: attacker.shields})

    updates.push({ 
        update: 'current', 
        type: attacker.name, 
        value: attackerCurrent})

    updates.push({
        update: 'afflictions',
        type: attacker.name,
        value: attacker.afflictions
    })

    let opponentGuessValue = attackeeHadReflect ? 0 : guessValue * 2
    
    updates.push({ 
        update: 'shields', 
        type: attackee.name, 
        value: Math.max(0, attackee.shields - opponentGuessValue)})

    let remainingAttack = Math.max(0, opponentGuessValue - attackee.shields)

    let attackeeCurrent = Math.max(0, attackee.current - remainingAttack)
    
    updates.push({ 
        update: 'current', 
        type: attackee.name, 
        value: attackeeCurrent})

    updates.push({
        update: 'afflictions',
        type: attackee.name,
        value: hasPoisonDagger(attacker.equipped) && opponentGuessValue > 0 && !attackeeAfflictions.includes('poison') ? 
            [...attackeeAfflictions, 'poison'] : attackeeAfflictions
    })

    return updates
}

export const processReflectiveShield = (unit: unit, guessValue: number) => {
    return [
        { update: 'shields', type: unit.name, value: guessValue >= 4 ? unit.shields + unit.level : unit.shields },
        { 
            update: 'afflictions', 
            type: unit.name, 
            value: guessValue >= 4 && !unit.afflictions.includes('reflect') ? 
                [ ...unit.afflictions, "reflect"] : unit.afflictions
        }
    ]
}

export const getUpdate = (type: string, update: string, updates: any[]) => {
    for (let item of updates) {
        if (item.update === update && item.type === type) {
            return item.value
        }
    }

    return 0
}