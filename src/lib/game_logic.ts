import { unit } from "./localStorage"
import { getCorrectLetterGuesses, getGuessValue, getUnlockableLetter } from "./statuses"

import sword from '../assets/dagger.png'
import poison_sword from '../assets/store/PoisonDagger.png'
import shield from '../assets/shield_new.png'
import medicine from '../assets/store/Medicine.png'
import jester from '../assets/store/JesterHat.png'
import reflect from "../assets/store/ReflectiveShield.png"
import { getUpdate, processAttack, processHeal, processJester, processReflectiveShield, processShields } from "./battleLogic"
import { chooseAiAction, chooseWord } from "./ai"

export type TurnData = {
    action: string
    guess: string
    guessValue: number
    guesses: string[]
    solution: string
    player: unit
    enemy: unit
}

export type TurnSummary = {
    guess: string
    player: unit
    enemy: unit
    player_animation: string
    enemy_animation: string
}

const processGuess = async (data: TurnData) => {
    let player = data.player
    let level = player.level

    let standardGuess = data.guess
    let standardGuessValue = data.guessValue * level

    if (player.equipped.includes('LuckyRoll')) {
      standardGuess = processLuckyRoll(data)
      standardGuessValue = getGuessValue(standardGuess, data.guesses)
    }
    data = {
        ...data,
        guess: standardGuess
    }
    let correctLettersValue = getCorrectLetterGuesses(standardGuess, data.guesses) * level

    if (player.action === sword || player.action === poison_sword) {
        return handleAttack(data, true, standardGuessValue)
    } else if (player.action === shield) {
        return handleShields(data, true, correctLettersValue)
    } else if (player.action === medicine) {
        return handleHeal(data, true, standardGuessValue)
    } else if (player.action === jester) {
        return handleJester(data, true, standardGuessValue)
    } else if (player.action === reflect) {
        return handleReflectiveShields(data, true, standardGuessValue)
    }

    return {
        guess: data.guess,
        player: data.player,
        enemy: data.enemy,
        player_animation: "",
        enemy_animation: ""
    }
}

const processEnemyGuess = async (data: TurnData) => {

    let aiAction = chooseAiAction(data.enemy, data.guesses)
    let aiGuess = chooseWord(data.guesses)

    data = {
        ...data,
        guess: aiGuess,
        guessValue: getGuessValue(aiGuess.toUpperCase(), data.guesses),
        enemy: {
            ...data.enemy,
            action: aiAction
        }
    }
    
    let enemy = data.enemy
    let level = enemy.level

    let standardGuess = data.guess
    let standardGuessValue = data.guessValue * level

    if (enemy.equipped.includes('LuckyRoll')) {
      standardGuess = processLuckyRoll(data)
      standardGuessValue = getGuessValue(standardGuess, data.guesses)
    }
    data = {
        ...data,
        guess: standardGuess
    }
    let correctLettersValue = getCorrectLetterGuesses(standardGuess, data.guesses) * level

    if (enemy.action === sword || enemy.action === poison_sword) {
        return handleAttack(data, false, standardGuessValue)
    } else if (enemy.action === shield) {
        return handleShields(data, false, correctLettersValue)
    } else if (enemy.action === medicine) {
        return handleHeal(data, false, standardGuessValue)
    } else if (enemy.action === jester) {
        return handleJester(data, false, standardGuessValue)
    } else if (enemy.action === reflect) {
        return handleReflectiveShields(data, false, standardGuessValue)
    }

    return {
        guess: data.guess,
        player: data.player,
        enemy: data.enemy,
        player_animation: "",
        enemy_animation: ""
    }
}

const processLuckyRoll = (data: TurnData): string => {
    let randomValue = Math.floor(Math.random() * 100)

    if (randomValue > 95) {
      let unlockIndex = getUnlockableLetter([...data.guesses, data.guess])

      return data.guess.substring(0, unlockIndex) + 
        data.solution.split('')[unlockIndex] + 
        data.guess.substring(unlockIndex + 1)
    }

    return data.guess
}

const handleAttack = (data: TurnData, playerTurn: boolean, guessValue: number) => {
    let summary : TurnSummary = {
        guess: data.guess,
        player: data.player,
        enemy: data.enemy,
        player_animation: "",
        enemy_animation: ""
    }

    let updates = []
    if (playerTurn) {
      updates = processAttack(summary.player, summary.enemy, guessValue)
      if (guessValue > 0) {
        summary = {
            ...summary,
            enemy_animation: "attack"
        }
      }
    } else {
      updates = processAttack(summary.enemy, summary.player, guessValue)
      if (guessValue > 0) {
        summary = {
            ...summary,
            player_animation: "attack"
        }
      }
    }

    return {
        ...summary,
        player: {
            ...summary.player,
            shields: getUpdate('player', 'shields', updates),
            current: Math.max(0, getUpdate('player', 'current', updates)),
            afflictions: getUpdate('player', 'afflictions', updates)
        },
        enemy: {
            ...summary.enemy,
            shields: getUpdate('enemy', 'shields', updates),
            current: Math.max(0, getUpdate('enemy', 'current', updates)),
            afflictions: getUpdate('enemy', 'afflictions', updates)
        }
    }
}

const handleShields = (data: TurnData, playerTurn: boolean, guessValue: number) => {
    let summary : TurnSummary = {
        guess: data.guess,
        player: data.player,
        enemy: data.enemy,
        player_animation: "",
        enemy_animation: ""
    }

    let updates = []
    if (playerTurn) {
      updates = processShields(summary.player, guessValue)
      if (guessValue >= 2) {
        summary = {
            ...summary,
            player_animation: "shield"
        }
      }
    } else {
      updates = processShields(summary.enemy, guessValue)
      if (guessValue >= 2) {
        summary = {
            ...summary,
            enemy_animation: "shield"
        }
      }
    }

    return {
        ...summary,
        player: {
            ...summary.player,
            shields: playerTurn ? getUpdate('player', 'shields', updates) : summary.player.shields,
            current: Math.max(0, summary.player.current)
        },
        enemy: {
            ...summary.enemy,
            shields: !playerTurn ? getUpdate('enemy', 'shields', updates) : summary.enemy.shields,
            current: Math.max(0, summary.enemy.current)
        }
    }
}

const handleHeal = (data: TurnData, playerTurn: boolean, guessValue: number) => {
    let summary : TurnSummary = {
        guess: data.guess,
        player: data.player,
        enemy: data.enemy,
        player_animation: "",
        enemy_animation: ""
    }

    let updates = []

    if (playerTurn) {
      updates = processHeal(summary.player, guessValue)
      if (guessValue > 0) {
        summary = {
            ...summary,
            player_animation: "heal"
        }
      }
    } else {
      updates = processHeal(summary.enemy, guessValue)
      if (guessValue > 0) {
        summary = {
            ...summary,
            enemy_animation: "heal"
        }
      }
    }

    let playerCurrent = playerTurn ? getUpdate('player', 'current', updates) : summary.player.current
    let enemyCurrent = !playerTurn ? getUpdate('enemy', 'current', updates) : summary.enemy.current

    return {
        ...summary,
        player: {
            ...summary.player,
            current: Math.max(0, playerCurrent),
            bandages: playerTurn ? getUpdate('player', 'bandages', updates) : summary.player.bandages,
            action: playerTurn ? getUpdate('player', 'action', updates) :summary.player.action
        },
        enemy: {
            ...summary.enemy,
            current: Math.max(0, enemyCurrent),
            bandages: !playerTurn ? getUpdate('enemy', 'bandages', updates) : summary.enemy.bandages,
            action: !playerTurn ? getUpdate('enemy', 'action', updates) : summary.enemy.action,
        }
    }
}

const handleJester = (data: TurnData, playerTurn: boolean, guessValue: number) => {
    let summary : TurnSummary = {
        guess: data.guess,
        player: data.player,
        enemy: data.enemy,
        player_animation: "",
        enemy_animation: ""
    }

    let updates = []
    if (playerTurn) {
      updates = processJester(summary.player, summary.enemy, guessValue)

      if (guessValue > 0) {
        summary = {
            ...summary,
            player_animation: "attack",
            enemy_animation: "attack"
        }
      }
    } else {
      updates = processJester(summary.enemy, summary.player, guessValue)

      if (guessValue > 0) {
        summary = {
            ...summary,
            player_animation: "attack",
            enemy_animation: "attack"
        }
      }
    }

    return {
        ...summary,
        player: {
            ...summary.player,
            shields: getUpdate('player', 'shields', updates),
            current: Math.max(0, getUpdate('player', 'current', updates)),
            afflictions: getUpdate('player', 'afflictions', updates)
        },
        enemy: {
            ...summary.enemy,
            shields: getUpdate('enemy', 'shields', updates),
            current: Math.max(0, getUpdate('enemy', 'current', updates)),
            afflictions: getUpdate('enemy', 'afflictions', updates)
        }
    }
}

const handleReflectiveShields = (data: TurnData, playerTurn: boolean, guessValue: number) => {
    let summary : TurnSummary = {
        guess: data.guess,
        player: data.player,
        enemy: data.enemy,
        player_animation: "",
        enemy_animation: ""
    }

    let updates = []
    if (playerTurn) {
      updates = processReflectiveShield(summary.player, guessValue)
      if (guessValue >= 4) {
        summary = {
            ...summary,
            player_animation: "shield"
        }
      }
    } else {
      updates = processReflectiveShield(summary.enemy, guessValue)
      if (guessValue >= 4) {
        summary = {
            ...summary,
            enemy_animation: "shield"
        }
      }
    }

    return {
        ...summary,
        player: {
            ...summary.player,
            hields: playerTurn ? getUpdate('player', 'shields', updates) : summary.player.shields,
            current: Math.max(0, summary.player.current),
            afflictions: playerTurn ? getUpdate('player', 'afflictions', updates) : summary.player.afflictions
        },
        enemy: {
            ...summary.enemy,
            shields: !playerTurn ? getUpdate('enemy', 'shields', updates) : summary.enemy.shields,
            current: Math.max(0, summary.enemy.current),
            afflictions: !playerTurn ? getUpdate('enemy', 'afflictions', updates) : summary.enemy.afflictions
        }
    }
}

export {
    processGuess,
    processEnemyGuess
}