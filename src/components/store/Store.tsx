import { useShallow } from 'zustand/react/shallow'
import { unit } from "../../lib/localStorage"
import { Shelf } from "./Shelf"
import { useBattleStore } from '../../game_state'

import { Cell } from '../grid/Cell'
import male_4 from "../../assets/avatars/male_4.png"
import { CharacterCell } from "../battle/CharacterCell"
import { useState } from "react"

import { purchaseData } from './Items'

type Props = {
    leave: () => void,
}

const SERVICES = [ 'Heal', 'Cure', 'UpgradeHealth', 'LevelUp' ]
const ITEMS = [ 'Medicine', 'PoisonDagger', 'VampireCloak', 'GoldPot', 'ReflectiveShield', 'LuckyRoll', 'JesterHat', 'SpeedyBoots' ]

const removeBoughtItems = (player: unit, items: string[]): string[] => {
    let availableItems = []

    // let hasPoisonDagger = battleState.player.items.includes('PoisonDagger')
    let hasPoisonDagger2 = player.items.includes('PoisonDagger2')

    for (let item of items) { 
        if (item === 'PoisonDagger' && hasPoisonDagger2) continue

        if (!player.items.includes(item)) {
            availableItems.push(item)
        }
    }

    return availableItems
}

const DEFAULT_ITEM = {
    id: "None", 
    title: "None", 
    purchaseText: "Purchase", 
    purchaseDescription: "Relax weary travelers, the night is long and full of knifers. No KNIFING at the Knifers Delight", 
    price: 0
}
 
export const Store = ({ leave }: Props) => {
    const { money, player, setMoney, setPlayer } = useBattleStore(
        useShallow((state) => ({ money: state.money, player: state.player, 
            setMoney: state.setMoney, setPlayer: state.setPlayer }))
    )

    const [selectedPurchase, setSelectedPurchase] = useState<purchaseData>(DEFAULT_ITEM)

    const healPlayer = () => {
        setPlayer({
            ...player,
            current: player.total
        })
        setMoney(money - 10)
    }

    const upgradeStat = (data: purchaseData) => {
        if (data.id === 'UpgradeHealth') {
          setPlayer({
            ...player,
            current: player.current + 1,
            total: player.total + 1
          })
          setMoney(money - data.price)
        } else if (data.id === 'LevelUp') {
          setPlayer({
            ...player,
            level: player.level + 1
          })
          setMoney(money - data.price)
        }
    }

    const purchaseItem = (data: purchaseData) => {
        setMoney(money - data.price)
        if (data.id === 'Medicine') {
          setPlayer({
            ...player,
            bandages: player.bandages + 3
          })
          return 
        }
    
        if (player.items.includes(data.id.substring(0, data.id.length - 1))) {
          setPlayer({
            ...player,
            items: [...player.items.filter((item) => item !== data.id.substring(0, data.id.length - 1)), data.id]
          })
          return
        }
    
        setPlayer({
          ...player,
          items: [...player.items, data.id],
        })
      }

    const curePlayer = () => {
        setPlayer({
            ...player,
            afflictions: player.afflictions.includes('vampire') ? ['vampire'] : []
        })
        setMoney(money - 10)
    }
    
    const purchaseClicked = () => {
        if (selectedPurchase.id === 'Heal') {
            attemptPurchase(selectedPurchase.price, healPlayer)
        } else if (selectedPurchase.id === 'Cure') {
            attemptPurchase(selectedPurchase.price, curePlayer)
        } else if (selectedPurchase.id === 'Medicine') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        } else if (selectedPurchase.id === 'UpgradeHealth') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, upgradeStat)
        } else if (selectedPurchase.id === 'LevelUp') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, upgradeStat)
        } else if (selectedPurchase.id === 'PoisonDagger') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        } else if (selectedPurchase.id === 'VampireCloak') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        } else if (selectedPurchase.id === 'LuckyRoll') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        } else if (selectedPurchase.id === 'JesterHat') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        } else if (selectedPurchase.id === 'SpeedyBoots') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        } else if (selectedPurchase.id === 'GoldPot') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        } else if (selectedPurchase.id === 'PoisonDagger2') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        } else if (selectedPurchase.id === 'ReflectiveShield') {
            attemptPurchaseWithData(selectedPurchase.price, selectedPurchase.id, purchaseItem)
        }
    }

    const attemptPurchase = (cost: number, purchaseCall: () => void) => {
        if (money < cost) {
            setSelectedPurchase({
                ...selectedPurchase,
                purchaseDescription: "With what money? I see no cash."
            })
            return
        }

        purchaseCall()
        setSelectedPurchase(DEFAULT_ITEM)
    }

    const attemptPurchaseWithData = (cost: number, id: string, purchaseCall: (data: purchaseData) => void) => {
        if (money < cost) {
            setSelectedPurchase({
                ...selectedPurchase,
                purchaseDescription: "With what money? I see no cash."
            })
            return
        }

        if (player.items.includes(id)) {
            setSelectedPurchase({
                ...selectedPurchase,
                purchaseDescription: "Really you want to buy two of the same item? Get out of here, don't make me save you from yourself"
            })
            return
        }

        purchaseCall(selectedPurchase)
        setSelectedPurchase(DEFAULT_ITEM)
    }

	return (
	<div className="max-w-7xl mx-auto overflow-x-hidden sm:px-6 lg:px-8">
        <div className="flex justify-center mb-1 mt-4">
          <Cell value="K" status="correct" />
          <Cell value="N" status="correct" />
          <Cell value="I" status="correct" />
          <Cell value="F" status="correct"/>
          <Cell value="E" status="correct"/>
          <Cell value="R" status="correct"/>
          <Cell value="S" status="correct" />
          <Cell value="" />
        </div>
        <div className="flex justify-center mt-2">
          <Cell value="" />
          <Cell value="D" status="correct" />
          <Cell value="E" status="correct" />
          <Cell value="L" status="correct"/>
          <Cell value="I" status="correct"/>
          <Cell value="G" status="correct"/>
          <Cell value="H" status="correct"/>
          <Cell value="T" status="correct" />
        </div>
        <div className="flex justify-left px-5 mx-8 my-6">
            <div className="scale-150 mt-2">
                <div className="scale-150">
                    <img className="scale-150" src={male_4} alt="" />
                </div>
            </div>
        </div>
        <div className="flex justify-center mx-8 my-2">
            <p className="text-xs text-gray-500 dark:text-gray-300 mt-1">
                {selectedPurchase.purchaseDescription}
            </p>
        </div>
        {selectedPurchase.id !== "None" && (
            <>
                <div className="flex justify-center mx-8 mt-4">
                    <p className="text-xs text-gray-500 dark:text-gray-300 mt-1">
                        {selectedPurchase.title}
                    </p>
                </div>
                <button
                    type="button"
                    className="mx-auto mt-4 flex items-center px-2 py-2 border border-transparent font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 select-none"
                    onClick={purchaseClicked}
                >
                    {selectedPurchase.purchaseText}
                </button>
            </>
        )}
        <div className="flex justify-center">
            <CharacterCell unit={player} animation={""} stopAnimation={() => null} onClick={() => null} />
        </div>
        <div className="flex justify-center mt-4">
            <p className="text-xs text-gray-500 dark:text-gray-300 mt-2">
                {"$" + money}
            </p>
        </div>
        <Shelf unit={player} title={"Services & Upgrades"} items={SERVICES} select={setSelectedPurchase} />
        <Shelf unit={player} title={"Items"} items={removeBoughtItems(player, ITEMS)} select={setSelectedPurchase} />
        <button
            type="button"
            className="mx-auto mt-6 flex items-center px-2 py-2 border border-transparent font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 select-none"
            onClick={leave}
        >
            {"Leave Store"}
        </button>
	</div>
	)
}
  