import { BaseModal } from './BaseModal'
import { Actions } from '../battle/Actions'

type Props = {
  isOpen: boolean
  handleClose: () => void,
  selectAction: (value: string) => void,
}

export const ActionModal = ({ isOpen, handleClose, selectAction }: Props) => {
  return (
    <BaseModal title="Choose Action" isOpen={isOpen} handleClose={handleClose}>
      <Actions onClick={selectAction} />
    </BaseModal>
  )
}
