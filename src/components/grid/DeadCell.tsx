import classnames from 'classnames'


export const DeadCell = () => {
  const classes = classnames(
    'w-14 h-14 visibility:hidden'
  )

  return <div className={classes}></div>
}
