import { BattleState } from '../../lib/localStorage'
import { MAX_WORD_LENGTH } from '../../constants/settings'
import { Cell } from './Cell'
import { ActionCell } from './ActionCell'
import { useBattleStore } from '../../game_state'
import { useShallow } from 'zustand/react/shallow'

type Props = {
  guess: string,
  actionClick: (player: boolean) => void,
}

export const CurrentRow = ({ guess, actionClick }: Props) => {
  const { playerTurn, playerAction, enemyAction } = useBattleStore(
    useShallow((state) => ({ playerTurn: state.playerTurn, playerAction: state.player.action, enemyAction: state.enemy.action }))
  )
  const splitGuess = guess.split('')
  const emptyCells = Array.from(Array(MAX_WORD_LENGTH - splitGuess.length))

  return (
    <div className="flex justify-center mb-1">
      <ActionCell key={0} player={true} active={playerTurn} action={playerAction} onClick={actionClick} />
      {splitGuess.map((letter, i) => (
        <Cell key={i+1} value={letter} />
      ))}
      {emptyCells.map((_, i) => (
        <Cell key={i+1} />
      ))}
      <ActionCell key={7} player={false} active={!playerTurn} action={enemyAction} onClick={actionClick} />
    </div>
  )
}
