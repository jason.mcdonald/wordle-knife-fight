
import { MAX_CHALLENGES } from '../../constants/settings'
import { CompletedRow } from './CompletedRow'
import { CurrentRow } from './CurrentRow'
import { EmptyRow } from './EmptyRow'
import { getGuessValue } from '../../lib/statuses'
 
type Props = {
  guesses: string[]
  guessOrder: string[]
  currentGuess: string,
  chooseAction: () => void,
}

export const OverflowGrid = ({ guesses, guessOrder, currentGuess, chooseAction }: Props) => {
  const localGuesses = guesses.length < 5 ? guesses : [...guesses].splice(-5)
  const empties =
  localGuesses.length < MAX_CHALLENGES - 1
      ? Array.from(Array(MAX_CHALLENGES - 1 - localGuesses.length))
      : []
  
  const actionClick = (player: boolean) => {
    if (!player) {
      return
    }

    chooseAction()
  }

  return (
    <div className="pb-2">
      {localGuesses.map((guess, i) => (
        <CompletedRow key={i} guess={guess} order={guessOrder[i]} guessValue={getGuessValue(guess, guesses.slice(0, i))} />
      ))}
      <CurrentRow guess={currentGuess} actionClick={actionClick} />
      {empties.map((_, i) => (
        <EmptyRow key={i} />
      ))}
    </div>
  )
}
