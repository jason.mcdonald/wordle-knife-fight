import { useShallow } from 'zustand/react/shallow'
import { AvatarSelection } from "./components/avatar/AvatarSelction"
import { useBattleStore } from "./game_state"
import { unit } from "./lib/localStorage"

type Props = {
	play: () => void,
	continueGame: () => void,
	updateAvatar: (value: string) => void,
}

export const Menu = ({ play, continueGame, updateAvatar }: Props) => {

	const { round, player, enemy } = useBattleStore(
        useShallow((state) => ({ round: state.round, player: state.player, enemy: state.enemy }))
    )

	const hasCurrentGame = (round: number, player: unit, enemy: unit) => {
		if (player.current <= 0) return false

		return round > 0 || player.current < player.total || enemy.current < enemy.total
	}

	return (
		<div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
			<AvatarSelection current={player.avatar} updateAvatar={updateAvatar} />
			<button
				type="button"
				className="mx-auto mt-8 flex items-center px-5 py-3 border border-transparent text-2xl font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 select-none"
				onClick={play}
			>
				{"New Game"}
			</button>
			{ hasCurrentGame(round, player, enemy) && (
				<button
					type="button"
					className="mx-auto mt-8 flex items-center px-5 py-3 border border-transparent text-2xl font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 select-none"
					onClick={continueGame}
				>
					{"Continue"}
				</button>
			)}
		</div>
	)
}
  