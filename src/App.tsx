import { useState, useEffect } from 'react'
import { Banner } from './components/banner/Banner'
import { AboutModal } from './components/modals/AboutModal'
import { InfoModal } from './components/modals/InfoModal'
import { AudioModal } from "./components/modals/AudioModal"
import {
  ABOUT_GAME_MESSAGE,
} from './constants/strings'

import './App.css'
import Game from './Game'

function App() {
  const [isInfoModalOpen, setIsInfoModalOpen] = useState(true)
  const [isAboutModalOpen, setIsAboutModalOpen] = useState(false)
  const [isAudioModalOpen, setIsAudioModalOpen] = useState(false)

  const [music] = useState(new Audio('celtic.mp3'));

  const [playing, setPlaying] = useState(false);
  const [playSounds, setPlaySounds] = useState(false);

  useEffect(() => {
      playing ? music.play() : music.pause();
    },[playing, music]);

  useEffect(() => {
    music.addEventListener('ended', () => music.play());
  }, []);

  return (
    <div className="py-4 max-w-7xl mx-auto sm:px-6 lg:px-8">
      <Banner audioOn={playing || playSounds} showAudioSettings={() => setIsAudioModalOpen(true)} />
      <Game soundEffects={playSounds} />
      <InfoModal
        isOpen={isInfoModalOpen}
        handleClose={() => setIsInfoModalOpen(false)}
      />
      <AboutModal 
        isOpen={isAboutModalOpen}
        handleClose={() => setIsAboutModalOpen(false)}
      />
      <AudioModal 
        background={playing}
        sound={playSounds}
        setBackground={setPlaying}
        setSound={setPlaySounds}
        isOpen={isAudioModalOpen} 
        handleClose={() => setIsAudioModalOpen(false)} 
        />
      <button
        type="button"
        className="mx-auto mt-8 flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 select-none"
        onClick={(e) => {
          e.currentTarget.blur()
          setIsAboutModalOpen(true)
        }}
      >
        {ABOUT_GAME_MESSAGE}
      </button>
    </div>
  )
}

export default App
