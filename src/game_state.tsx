import { create } from 'zustand';
import { persist } from 'zustand/middleware'

import { BattleState, StoredGameState } from './lib/localStorage';

import {
    loadBattleStateFromLocalStorage,
    loadGameStateFromLocalStorage,
    loadRandomSeedFromLocalStorage,
    unit
  } from './lib/localStorage'
  import { getWordFromSeed } from './lib/words'
import { BASE_ENEMY, BASE_PLAYER, createNewEnemy } from './constants/units';


const getLocalGameStateOrDefault = () => {
    const loaded = loadGameStateFromLocalStorage()
    const seed = loadRandomSeedFromLocalStorage()

    if (loaded?.solution !== getWordFromSeed(seed).solution) {
        return {
            seed: seed,
            guesses: [],
            guessOrder: [],
            solution: getWordFromSeed(seed).solution
        }
    }
    
    return loaded
}

const getLocalBattleStateOrDefault = () => {
    const loaded = loadBattleStateFromLocalStorage()

    if (!loaded) {
        return {
            playerTurn: true,
            round: 0,
            money: 30,
            player: BASE_PLAYER,
            enemy: BASE_ENEMY
        }
    }

    return loaded
}

const gameStateKey = 'gameState'
const battleStateKey = 'battleState'

const useGameStore = create<StoredGameState>((set) => ({
    seed: getLocalGameStateOrDefault().seed,
    guesses: getLocalGameStateOrDefault().guesses,
    guessOrder: getLocalGameStateOrDefault().guessOrder,
    solution: getLocalGameStateOrDefault().solution,
    setSeed: (newSeed: number) => set((state)  => {
        localStorage.setItem(gameStateKey, JSON.stringify({
            seed: newSeed,
            guesses: state.guesses,
            guessOrder: state.guessOrder,
            solution: state.solution
        }));
        return {
            seed: newSeed
        }
    }),
    setGuesses: (newGuesses: string[]) => set((state) => {
        localStorage.setItem(gameStateKey, JSON.stringify({
            seed: state.seed,
            guesses: newGuesses,
            guessOrder: state.guessOrder,
            solution: state.solution
        }));
        return {
            guesses: newGuesses
        }
    }),
    addGuess: (guess: string, order: string) => set((state) => { 
        localStorage.setItem(gameStateKey, JSON.stringify({
            seed: state.seed,
            guesses: [...state.guesses, guess],
            guessOrder: [...state.guessOrder, order],
            solution: state.solution
        }));
        return {
            guesses: [...state.guesses, guess],
            guessOrder: [...state.guessOrder, order]
        }
    }),
    setGuessOrder: (newGuessOrder: string[]) => set((state) => {
        localStorage.setItem(gameStateKey, JSON.stringify({
            seed: state.seed,
            guesses: state.guesses,
            guessOrder: newGuessOrder,
            solution: state.solution
        }));
        return {
            guessOrder: newGuessOrder
        }
    }),
    setSolution: (newSolution: string) => set((state) => {
        localStorage.setItem(gameStateKey, JSON.stringify({
            seed: state.seed,
            guesses: state.guesses,
            guessOrder: state.guessOrder,
            solution: newSolution
        }));
        return {
            solution: newSolution
        }
    }),
    newWord: () => set((state) => {
        const newSeed : number = Math.floor(Math.random() * 1000000)
        localStorage.setItem(gameStateKey, JSON.stringify({
            seed: newSeed,
            guesses: state.guesses,
            guessOrder: state.guessOrder,
            solution: getWordFromSeed(newSeed).solution
        }));
        return {
            seed: newSeed,
            guesses: [],
            guessOrder: [],
            solution: getWordFromSeed(newSeed).solution
        }
    }),
    persist
}));


const useBattleStore = create<BattleState>((set) => ({
    playerTurn: getLocalBattleStateOrDefault().playerTurn,
    round: getLocalBattleStateOrDefault().round,
    money: getLocalBattleStateOrDefault().money,
    player: getLocalBattleStateOrDefault().player,
    enemy: getLocalBattleStateOrDefault().enemy,
    setPlayerTurn: (turn: boolean) => set((state) => {
        localStorage.setItem(battleStateKey, JSON.stringify({
            playerTurn: turn,
            round: state.round,
            money: state.money,
            player: state.player,
            enemy: state.enemy,
        }));
        return {
            playerTurn: turn
        }
    }),
    setRound: (newRound: number) => set((state) => {
        localStorage.setItem(battleStateKey, JSON.stringify({
            playerTurn: state.playerTurn,
            round: newRound,
            money: state.money,
            player: state.player,
            enemy: state.enemy,
        }));
        return {
            round: newRound
        }
    }),
    setMoney: (newMoney: number) => set((state) => {
        localStorage.setItem(battleStateKey, JSON.stringify({
            playerTurn: state.playerTurn,
            round: state.round,
            money: newMoney,
            player: state.player,
            enemy: state.enemy,
        }));
        return {
            money: newMoney
        }
    }),
    setPlayer: (newPlayer: unit) => set((state) => {
        localStorage.setItem(battleStateKey, JSON.stringify({
            playerTurn: state.playerTurn,
            round: state.round,
            money: state.money,
            player: newPlayer,
            enemy: state.enemy,
        }));
        return {
            player: newPlayer
        }
    }),
    setEnemy: (newEnemy: unit) => set((state) => {
        localStorage.setItem(battleStateKey, JSON.stringify({
            playerTurn: state.playerTurn,
            round: state.round,
            money: state.money,
            player: state.player,
            enemy: newEnemy,
        }));
        return {
            enemy: newEnemy
        }
    }),
    nextRound: () => set((state) => {
        const newEnemy = createNewEnemy(state.round + 1)
        localStorage.setItem(battleStateKey, JSON.stringify({
            playerTurn: true,
            round: state.round + 1,
            money: state.money,
            player: state.player,
            enemy: newEnemy,
        }));
        return {
            playerTurn: true,
            round: state.round + 1,
            money: state.money,
            player: state.player,
            enemy: newEnemy,
        }
    }),
    newGame: (avatar: string) => set((state) => {
        localStorage.setItem(battleStateKey, JSON.stringify({
            playerTurn: true,
            round: 0,
            money: 30,
            player: { ...BASE_PLAYER, avatar: avatar },
            enemy: state.enemy,
        }));
        return {
            playerTurn: true,
            round: 0,
            money: 30,
            player: { ...BASE_PLAYER, avatar: avatar },
            enemy: createNewEnemy(0),
        }
    })
}))
  
export {
    useGameStore,
    useBattleStore
};